
const state = {
  gamePoint: 10
}

const mutations = {
  increaseGamePint(state , value) {
    state.gamePoint = value
  },
  decreaseGamePint(state) {
    state.gamePoint --
  },
}

export default {
  state,
  mutations
}
