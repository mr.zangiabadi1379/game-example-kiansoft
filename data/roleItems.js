export const roleItems = [
    {
        id: 1,
        icon: "/svg/cherry.svg",
        value:"cherry"
    },
    {
        id: 2,
        icon: "/svg/lemon.svg",
        value:"lemon"
    },
    {
        id: 3,
        icon: "/svg/orange.svg",
        value:"orange"
    },
    {
        id: 4,
        icon: "/svg/watermelon.svg",
        value:"watermelon"
    }
]